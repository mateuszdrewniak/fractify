# frozen_string_literal: true

require 'test_helper'

module Fractify
  class CalculatorTest < Minitest::Test
    def test_evaluate
      evaluate_nil_test('(-2) + (5) * (-5) + (1/2')
      evaluate_nil_test('(-2.0) + (5) ** (-5) + (0.5)')
      evaluate_nil_test('((-2.5) + (3.0))^(2.0.0) * (2.0) - (-0.5)')
      evaluate_nil_test('(3 1:3) : (5:6)')
      evaluate_nil_test('(3 1:3) ÷ (5/6)')
      evaluate_nil_test('(3 1/3) / (5:6)')
      evaluate_nil_test('((-2 1/2 2) + (3))^(2) * (2) - (-1/2)')
      evaluate_nil_test('(-2 1/2 + (3))^(2) * (2) - (-1/2)')
      evaluate_nil_test('(2)(1 1/2)')
      evaluate_nil_test('(2) * (1 1/2)(3)')
      evaluate_nil_test('(2) * (1 1/2)(3)')
      evaluate_nil_test('(2) * (1 1/2)34')

      evaluate_test('(-2) + (5) * (-5) + (1/2)', '(-26 1/2)')
      evaluate_test('(9) / (3)', '(3)')
      evaluate_test('(9) : (3)', '(3)')
      evaluate_test('(9) ÷ (3)', '(3)')
      evaluate_test('(3 1/3) / (5/6)', '(4)')
      evaluate_test('(3 1/3) : (5/6)', '(4)')
      evaluate_test('(3 1/3) ÷ (5/6)', '(4)')
      evaluate_test('(-2.0) + (5) * (-5) + (0.5)', '(-26 1/2)')
      evaluate_test('((-2.5) + (3.0))^(2.0) * (2.0) - (-0.5)', '(1)')
      evaluate_test('((-2 1/2) + (3))^(2) * (2) - (-1/2)', '(1)')
      evaluate_test('(-3) + (1/2) * ((2) * (4 1/2)^(2)) - (8) * (1/2)', '(13 1/4)')
      evaluate_test('(((7/8) + (3/4)^(2))^(2))^(2)', '(4 17697/65536)')
      evaluate_test('(((0.875) + (0.75)^(2.0))^(2))^(2)', '(4 17697/65536)')
      puts ' Calculator evaluate passes' if ENABLE_PUTS
    end

    def test_valid
      valid_test('(-2) + (5) * (-5) + (1/2)', true)
      valid_test('(9) / (3)', true)
      valid_test('(3 1/3) / (5/6)', true)
      valid_test('(9) : (3)', true)
      valid_test('(9) ÷ (3)', true)
      valid_test('(3 1/3) : (5/6)', true)
      valid_test('(3 1/3) ÷ (5/6)', true)
      valid_test('(-2.0) + (5) * (-5) + (0.5)', true)
      valid_test('((-2.5) + (3.0))^(2.0) * (2.0) - (-0.5)', true)
      valid_test('((-2 1/2) + (3))^(2) * (2) - (-1/2)', true)
      valid_test('(-3) + (1/2) * ((2) * (4 1/2)^(2)) - (8) * (1/2)', true)
      valid_test('(((7/8) + (3/4)^(2))^(2))^(2)', true)
      valid_test('(((0.875) + (0.75)^(2.0))^(2))^(2)', true)

      valid_test('(-2) + (5) * (-5) + (1/2', false)
      valid_test('(3 1:3) : (5:6)', false)
      valid_test('(3 1/3) : (5÷6)', false)
      valid_test('(3 1÷3) ÷ (5÷6)', false)
      valid_test('(-2.0) + (5) ** (-5) + (0.5)', false)
      valid_test('((-2.5) + (3.0))^(2.0.0) * (2.0) - (-0.5)', false)
      valid_test('((-2 1/2 2) + (3))^(2) * (2) - (-1/2)', false)
      valid_test('(-2 1/2 + (3))^(2) * (2) - (-1/2)', false)
      valid_test('-2 1/2 + (3)^(2) * (2) - (-1/2)', false)
      valid_test('(2)(1 1/2)', false)
      valid_test('(2)(1 1/2) + (3)', false)
      valid_test('(2) * (1 1/2)(3)', false)
      valid_test('(2) * (1 1/2)3434 3', false)
      valid_test('(2) * (1 1/2)34', false)
      valid_test('(2) * hhg(1 1/2)3434 3', false)
      valid_test('(2) * (1 1/2)w', false)
      valid_test('w(2) * (1 1/2)w', false)
      valid_test('a(2) * (1 1/2)', false)
      valid_test('gh', false)
      valid_test('(nowy)', false)
      puts ' Calculator valid passes' if ENABLE_PUTS
    end

    private

    def valid_test(expression, should_be_correct)
      if should_be_correct
        assert Fractify::Calculator.valid?(expression)
      else
        assert !Fractify::Calculator.valid?(expression)
      end
    end

    def evaluate_nil_test(expression)
      assert_nil Fractify::Calculator.evaluate(expression)
    end

    def evaluate_test(expression, output)
      assert_equal output, Fractify::Calculator.evaluate(expression).to_s
    end
  end
end
