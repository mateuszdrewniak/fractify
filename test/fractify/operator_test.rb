# frozen_string_literal: true

require 'test_helper'

module Fractify
  class OperatorTest < Minitest::Test
    def test_operator
      fraction = Fractify::Fraction.new(string: '(-2)')

      operator = Fractify::Operator.new('+', 2)
      operator.increase_rank!(2)
      assert_equal 4, operator.rank
      operator.decrease_rank!(2)
      assert_equal 2, operator.rank
      operator.decrease_rank!
      assert_equal 1, operator.rank
      operator.increase_rank!
      assert_equal 2, operator.rank
      assert operator.not_executed?
      operator.executed!
      assert !operator.not_executed?
      assert_nil operator.to_left
      assert_nil operator.to_right
      operator.to_left = fraction
      assert_equal fraction, operator.to_left
      assert_nil operator.to_right
      operator.to_right = fraction
      assert_equal fraction, operator.to_right
      assert_equal fraction, operator.to_left

      puts ' OperatorTest passes' if ENABLE_PUTS
    end

    private

    def operator(rank)
      Fractify::Operator.new('+', rank)
    end
  end
end
