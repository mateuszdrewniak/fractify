# frozen_string_literal: true

require 'test_helper'

module Fractify
  class OperatorArrayTest < Minitest::Test
    def test_operator_array
      operator_array = Fractify::OperatorArray.new
      operator = operator('+', 0)
      operator_array.push(operator)
      operator = operator('-', 0)
      operator_array.push(operator)
      operator = operator('*', 2)
      operator_array.push(operator)
      operator = operator('+', 3)
      operator_array.push(operator)
      operator = operator('^', 2)
      operator_array.push(operator)

      assert_equal '(+, 3), (*, 2), (^, 2), (+, 0), (-, 0)', operator_array.sort_descending.to_s
      assert_equal '(+, 0), (-, 0), (*, 2), (^, 2), (+, 3)', operator_array.sort_ascending.to_s

      operator_array.sort_descending!
      assert_equal '(+, 3), (*, 2), (^, 2), (+, 0), (-, 0)', operator_array.to_s
      operator_array.sort_ascending!
      assert_equal '(+, 0), (-, 0), (*, 2), (^, 2), (+, 3)', operator_array.to_s
      puts ' OperatorArrayTest passes' if ENABLE_PUTS
    end

    private

    def operator(char, rank)
      Fractify::Operator.new(char, rank)
    end
  end
end
