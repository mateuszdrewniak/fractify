# frozen_string_literal: true

require 'test_helper'

module Fractify
  class FractionTest < Minitest::Test
    def test_fraction_construction
      fraction = Fractify::Fraction.new
      assert_equal '(0)', fraction.to_s

      construction_test(0, 1, 1, '(1)')
      construction_test(0, 0, 0, '(0)')
      construction_test(0, 4, 1, '(4)')
      construction_test(4, 1, 1, '(4)')
      construction_test(0, 3, 4, '(3/4)')
      puts ' Fraction basic construction passes' if ENABLE_PUTS
    end

    def test_constuction_negativity
      construction_test(-1, -16, -8, '(-3)')
      construction_test(-1, -16, 8, '(3)')
      construction_test(1, -16, -8, '(3)')
      construction_test(-1, 16, -8, '(3)')
      # puts ' Fraction negativity construction passes'
    end

    def test_transformation_methods
      transformation_test(1, -16, 8, '(-3)', '(-3/1)', '(-1/3)')
      transformation_test(-1, 3, 9, '(-1 1/3)', '(-4/3)', '(-3/4)')
      transformation_test(0, 20, 8, '(2 1/2)', '(5/2)', '(2/5)')
      puts ' Fraction transformations passes' if ENABLE_PUTS
    end

    def test_construction_from_string
      string_construction_test('(-20)')
      string_construction_test('(1)')
      string_construction_test('(-3)')
      string_construction_test('(-0)', '(0)')
      string_construction_test('(-2 1/4)')
      string_construction_test('(-9/4)', '(-2 1/4)')
      string_construction_test('(9/4)', '(2 1/4)')
      string_construction_test('(1 -99/-4)', '(25 3/4)')
      puts ' Fraction construction from string passes' if ENABLE_PUTS
    end

    def test_construction_from_float_string
      string_construction_test('(2.2)', '(2 1/5)', 2.2)
      string_construction_test('(2.5)', '(2 1/2)', 2.5)
      string_construction_test('(-2.2)', '(-2 1/5)', -2.2)
      string_construction_test('(0.0)', '(0)', 0.0)
      string_construction_test('(2.0)', '(2)', 2.0)
      string_construction_test('(1000.0)', '(1000)', 1000.0)
      string_construction_test('(2.1456)', '(2 91/625)', 2.1456)
      puts ' Fraction construction from float string passes' if ENABLE_PUTS
    end

    def test_construction_from_float
      fraction = Fractify::Fraction.new(numeric: 2.2)
      assert_equal '(2 1/5)', fraction.to_s
      assert_equal 2.2, fraction.to_f
      float_construction_test(2.2, '(2 1/5)')
      float_construction_test(2.5, '(2 1/2)')
      float_construction_test(-2.2, '(-2 1/5)')
      float_construction_test(-2.5, '(-2 1/2)')
      float_construction_test(0.0, '(0)')
      float_construction_test(2.0, '(2)')
      float_construction_test(1000.0, '(1000)')
      float_construction_test(2.1456, '(2 91/625)')
      puts ' Fraction construction from float passes' if ENABLE_PUTS
    end

    def test_valid_syntax
      assert !Fractify::Fraction.valid?('(-2 -1-/-2)')
      assert !Fractify::Fraction.valid?('(-2 1/2')
      assert Fractify::Fraction.valid?('(-2 -1/-2)')
      assert Fractify::Fraction.valid?('(-2 1/2)')

      assert Fractify::Fraction.valid?('(1/2)')
      assert Fractify::Fraction.valid?('(-135/20)')
      assert Fractify::Fraction.valid?('(20)')
      assert Fractify::Fraction.valid?('(0)')

      assert !Fractify::Fraction.valid?('(2.0.0)')
      assert !Fractify::Fraction.valid?('(w)')
      assert !Fractify::Fraction.valid?('new_way')
      assert !Fractify::Fraction.valid?('new.way')
      assert !Fractify::Fraction.valid?('(2.0.0w)')
      assert !Fractify::Fraction.valid?('jkhjk(2.0)')
      assert !Fractify::Fraction.valid?('(2 1/2i)')
      assert !Fractify::Fraction.valid?('(2.0')
      assert !Fractify::Fraction.valid?('2.0)')
      assert !Fractify::Fraction.valid?('2.0')
      assert !Fractify::Fraction.valid?('(-2.-0)')
      assert !Fractify::Fraction.valid?('(.523)')

      assert Fractify::Fraction.valid?('(-2.5)')
      assert Fractify::Fraction.valid?('(2.0)')
      assert Fractify::Fraction.valid?('(2.5)')
      assert Fractify::Fraction.valid?('(1.2)')
      assert Fractify::Fraction.valid?('(0.125)')
      assert Fractify::Fraction.valid?('(10231343.1243324)')
      puts ' Fraction valid syntax passes' if ENABLE_PUTS
    end

    def test_multiplication_division
      fraction = new_fraction(0, 3, 4) * new_fraction(1, 1, 3)
      assert_equal '(1)', fraction.to_s

      fraction = new_fraction(0, 3, 4) * 3
      assert_equal '(2 1/4)', fraction.to_s

      fraction = new_fraction(0, 3, 4) * 3.5
      assert_equal '(2 5/8)', fraction.to_s

      fraction = new_fraction(0, 3, 4) * new_fraction(-1, 1, 6)
      assert_equal '(-7/8)', fraction.to_s
      fraction *= new_fraction(-1, 1, 6)
      assert_equal '(1 1/48)', fraction.to_s
      fraction /= new_fraction(-1, 1, 6)
      assert_equal '(-7/8)', fraction.to_s
      fraction /= 2
      assert_equal '(-7/16)', fraction.to_s
      fraction *= 4
      assert_equal '(-1 3/4)', fraction.to_s

      fraction = new_fraction(-1, 1, 6) / new_fraction(-1, 1, 6)
      assert_equal '(1)', fraction.to_s

      fraction = new_fraction(-1, 1, 6) / new_fraction(1, 1, 6)
      assert_equal '(-1)', fraction.to_s

      fraction = new_fraction(0, 8, 9) / new_fraction(1, 1, 3)
      assert_equal '(2/3)', fraction.to_s
      puts ' Fraction multiplication/division passes' if ENABLE_PUTS
    end

    def test_addition_substraction
      fraction = new_fraction(0, 6, 4) + new_fraction(0, 1, 2)
      assert_equal '(2)', fraction.to_s

      fraction = new_fraction(0, 5, 8) + new_fraction(0, 13, 16)
      assert_equal '(1 7/16)', fraction.to_s

      fraction = new_fraction(-2, 1, 4) - new_fraction(-1, 3, 5)
      assert_equal '(-13/20)', fraction.to_s

      fraction = new_fraction(-2, 1, 4) - new_fraction(1, 3, 5)
      assert_equal '(-3 17/20)', fraction.to_s

      fraction = new_fraction(-2, 1, 4) + new_fraction(-1, 3, 5)
      assert_equal '(-3 17/20)', fraction.to_s

      fraction = new_fraction(0, 0, 0) + new_fraction(-1, 3, 5)
      assert_equal '(-1 3/5)', fraction.to_s

      fraction = new_fraction(0, 0, 0) - new_fraction(-1, 3, 5)
      assert_equal '(1 3/5)', fraction.to_s

      fraction = new_fraction(-1, 3, 5) - new_fraction(0, 0, 0)
      assert_equal '(-1 3/5)', fraction.to_s

      fraction = new_fraction(-1, 3, 5) + new_fraction(0, 0, 0)
      assert_equal '(-1 3/5)', fraction.to_s

      fraction = new_fraction(-1, 3, 5) + 3
      assert_equal '(1 2/5)', fraction.to_s

      fraction = new_fraction(0, 4, 1) + 7.5
      assert_equal '(11 1/2)', fraction.to_s

      fraction = new_fraction(-1, 3, 5) + new_fraction(1, 3, 5)
      assert_equal '(0)', fraction.to_s

      fraction = new_fraction(1, 3, 5) - new_fraction(1, 3, 5)
      assert_equal '(0)', fraction.to_s

      fraction = new_fraction(0, 0, 0) - new_fraction(1, 3, 5)
      assert_equal '(-1 3/5)', fraction.to_s

      fraction = new_fraction(0, 4, 1) - new_fraction(0, 4, 1)
      assert_equal '(0)', fraction.to_s

      fraction = new_fraction(0, 4, 1) - 2.2
      assert_equal '(1 4/5)', fraction.to_s
      puts ' Fraction addition/substraction passes' if ENABLE_PUTS
    end

    def test_power_number
      fraction = new_fraction(1, 1, 3)
      fraction **= 2
      assert_equal '(1 7/9)', fraction.to_s

      fraction = new_fraction(1, 1, 3)
      fraction **= 2.0
      assert_equal '(1 7/9)', fraction.to_s

      fraction = new_fraction(1, 1, 3)
      fraction **= 2.6
      assert_equal '(2 5635701/50000000)', fraction.to_s

      fraction = new_fraction(-1, 1, 3)
      fraction **= 2.6
      assert_equal '(-2 5635701/50000000)', fraction.to_s

      fraction = new_fraction(1, 1, 3)
      fraction **= -1
      assert_equal '(3/4)', fraction.to_s

      fraction = new_fraction(0, -4, 3)
      fraction **= 3
      assert_equal '(-2 10/27)', fraction.to_s

      fraction = new_fraction(0, -4, 3)
      fraction **= -3
      assert_equal '(-27/64)', fraction.to_s

      fraction = new_fraction(0, -4, 3)
      fraction **= -2
      assert_equal '(9/16)', fraction.to_s

      fraction = new_fraction(0, -4, 1)
      fraction **= -2
      assert_equal '(1/16)', fraction.to_s

      fraction = new_fraction(0, -4, 1)
      fraction **= 2
      assert_equal '(16)', fraction.to_s

      fraction = new_fraction(0, -4, 1)
      fraction **= 2.5
      assert_equal '(-32)', fraction.to_s
      puts ' Fraction power number passes' if ENABLE_PUTS
    end

    def test_power_fraction
      fraction = new_fraction(1, 1, 3)
      fraction **= new_fraction(2, 1, 1)
      assert_equal '(1 7/9)', fraction.to_s

      fraction = new_fraction(1, 1, 3)
      fraction **= new_fraction(-1, 1, 1)
      assert_equal '(3/4)', fraction.to_s

      fraction = new_fraction(0, -4, 3)
      fraction **= new_fraction(3, 1, 1)
      assert_equal '(-2 10/27)', fraction.to_s

      fraction = new_fraction(0, -4, 3)
      fraction **= new_fraction(-3, 1, 1)
      assert_equal '(-27/64)', fraction.to_s

      fraction = new_fraction(0, -4, 3)
      fraction **= new_fraction(-2, 1, 1)
      assert_equal '(9/16)', fraction.to_s

      fraction = new_fraction(0, -4, 1)
      fraction **= new_fraction(-2, 1, 1)
      assert_equal '(1/16)', fraction.to_s

      fraction = new_fraction(0, -4, 1)
      fraction **= new_fraction(0, 4, 2)
      assert_equal '(16)', fraction.to_s
      puts ' Fraction power fraction passes' if ENABLE_PUTS
    end

    def test_to_float
      fraction = new_fraction(0, -4, 1)
      assert_equal(-4.0, fraction.to_f)

      fraction = new_fraction(0, 15, 20)
      assert_equal 0.75, fraction.to_f

      fraction = new_fraction(1, 11, 20)
      assert_equal 1.55, fraction.to_f

      fraction = new_fraction(21, -81, 99)
      assert_equal(-21.818181818181817, fraction.to_f)
      puts ' Fraction to_f passes' if ENABLE_PUTS
    end

    def test_to_integer
      fraction = new_fraction(0, -4, 1)
      assert_equal(-4, fraction.to_i)

      fraction = new_fraction(0, 15, 20)
      assert_equal 0, fraction.to_i

      fraction = new_fraction(1, 11, 20)
      assert_equal 1, fraction.to_i

      fraction = new_fraction(21, -81, 99)
      assert_equal(-21, fraction.to_i)
      puts ' Fraction to_i passes' if ENABLE_PUTS
    end

    private

    def new_fraction(whole_part, numerator, denominator)
      Fractify::Fraction.new(
        whole_part: whole_part,
        numerator: numerator,
        denominator: denominator
      )
    end

    def float_construction_test(float, output)
      fraction = Fractify::Fraction.new(numeric: float)
      assert_equal output, fraction.to_s
      assert_equal float, fraction.to_f
    end

    def string_construction_test(input, output = input, float = false)
      fraction = Fractify::Fraction.new(string: input)
      assert_equal output, fraction.to_s
      assert_equal fraction.to_s, Fractify::Fraction.new(string: fraction.to_s).to_s
      assert_equal float, fraction.to_f if float
    end

    def construction_test(whole_part, numerator, denominator, output)
      fraction = new_fraction(whole_part, numerator, denominator)
      assert_equal output, fraction.to_s
    end

    def transformation_test(whole_part, numerator, denominator, output, improper, reciprocal)
      fraction = new_fraction(whole_part, numerator, denominator)

      assert_equal output, fraction.to_s
      assert_equal improper, fraction.to_s(true)
      fraction.simplify!
      assert_equal output, fraction.to_s
      fraction.to_reciprocal!
      assert_equal reciprocal, fraction.to_s
    end
  end
end
