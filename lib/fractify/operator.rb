# frozen_string_literal: true

module Fractify
  class IncorrectArgumentsError < StandardError; end
  class Operator
    attr_accessor :operator_character, :rank, :executed, :to_left, :to_right

    def initialize(operator_character, rank, to_left = nil, to_right = nil)
      if to_left && !to_left.is_a?(Fractify::Fraction) || to_right && !to_right.is_a?(Fractify::Fraction)
        raise IncorrectArgumentsError
      end

      self.operator_character = operator_character
      self.rank = rank
      self.to_left = to_left
      self.to_right = to_right
      self.executed = false
    end

    def <=>(other)
      raise IncorrectArgumentsError unless other.is_a? Fractify::Operator
      return 0 if rank == other.rank
      return 1 if rank > other.rank

      -1
    end

    def to_s
      "(#{operator_character}, #{rank})"
    end

    def decrease_rank!(number = 1)
      @rank -= number
    end

    def increase_rank!(number = 1)
      @rank += number
    end

    def executed!
      self.executed = true
    end

    def executed?
      executed
    end

    def not_executed?
      !executed
    end
  end
end
