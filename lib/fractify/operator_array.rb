# frozen_string_literal: true

module Fractify
  class OperatorArray < Array
    def sort_descending!
      sort! { |a, b| b <=> a }
    end

    def sort_descending
      Fractify::OperatorArray.new(sort { |a, b| b <=> a })
    end

    def sort_ascending!
      sort! { |a, b| a <=> b }
    end

    def sort_ascending
      Fractify::OperatorArray.new(sort { |a, b| a <=> b })
    end

    def to_s
      string = ''
      each do |a|
        string += a.to_s
        string += ', ' if a != last
      end
      string
    end
  end
end
