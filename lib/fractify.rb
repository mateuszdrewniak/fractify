# frozen_string_literal: true

Gem.find_files('fractify/*.rb').each { |path| require path }

module Fractify
  class Error < StandardError; end
end
